#!/bin/bash
#Install Splunk
cd /tmp
wget https://devsecops-nk2wnafwmn7e.s3.eu-central-1.amazonaws.com/splunk-7.3.0-657388c7a488-linux-2.6-x86_64.rpm
sudo yum -y install splunk*.rpm
cd /opt/splunk/bin/
sudo ./splunk enable boot-start --accept-license --answer-yes --no-prompt --seed-passwd password

#Switch to the free license
sudo echo "[license]
active_group = Free" >> /opt/splunk/etc/system/local/server.conf

#Add Imperva splunk apps for both rasp and cloud waf
sudo tar -zxvf /tmp/rasp-splunk-app.tgz -C /opt/splunk/etc/apps/
sudo tar -zxvf /tmp/imperva-appsecurity-view.tgz -C /opt/splunk/etc/apps/

#Add a receiver for the apps' splunk forwarders and increase minimum free disk space for indexing
sudo ./splunk enable listen 9997 -auth admin:password
sudo ./splunk set minfreemb 100




